/*
   Provides an interface to the ML smoothed Aggregation
   Note: Something non-obvious breaks -pc_mg_type ADDITIVE for parallel runs
                                    Jed Brown, see [PETSC #18321, #18449].
*/
#include <petsc/private/pcimpl.h>   /*I "petscpc.h" I*/
#include <petsc/private/pcmgimpl.h>                    /*I "petscksp.h" I*/
#include <../src/mat/impls/aij/seq/aij.h>
#include <../src/mat/impls/aij/mpi/mpiaij.h>
#include <petscdm.h>            /* for DMDestroy(&pc->mg) hack */

/*
EXTERN_C_BEGIN
#if !defined(HAVE_CONFIG_H)
#define HAVE_CONFIG_H
#endif
EXTERN_C_END
*/

#include <raptor.hpp>

using namespace std;

/*
// Function template
static PetscErrorCode functionName()
{
  PetscErrorCode    ierr=0;
  
  PetscFunctionBegin;
  
  CHKERRQ( callingAnotherFunction() );
  
  PetscFunctionReturn(ierr);
}

*/

/* The context (data structure) at each grid level */
typedef struct {
  Vec x,b,r;                  /* global vectors */
  Mat A,P,R;
  KSP ksp;
  Vec coords;                 /* projected by RAPTOR, if PCSetCoordinates is called; values packed by node */
} GridCtx;

/* The context used to input PETSc matrix into RAPTOR at fine grid */
typedef struct {
  Mat         A;       /* Petsc matrix in aij format */
  Mat         Aloc;    /* local portion of A to be used by RAPTOR */
  Vec         x,y;
  ParCSRMatrix *raptor_mat;
  //PetscScalar *pwork;  /* tmp array used by PetscML_comm() */
} FineGridCtx;

/* The context associates a raptor matrix with a PETSc shell matrix */
typedef struct {
  Mat         A;              /* PETSc shell matrix associated with mlmat */
  ParCSRMatrix *raptor_mat;   /* RAPTOR matrix assorciated with A */
  Vec         y, work;
} Mat_RAPTORShell;

/* Private context for the RAPTOR preconditioner */
typedef struct {
  //ML                *ml_object;
  //ML_Aggregate      *agg_object;
  GridCtx           *gridctx;
  FineGridCtx       *PetscRAPTORdata;
  PetscInt          Nlevels,MaxNlevels,MaxCoarseSize,CoarsenScheme,EnergyMinimization,MinPerProc,PutOnSingleProc,RepartitionType,ZoltanScheme;
  PetscReal         Threshold,DampingFactor,EnergyMinimizationDropTol,MaxMinRatio,AuxThreshold;
  PetscBool         SpectralNormScheme_Anorm,BlockScaling,EnergyMinimizationCheap,Symmetrize,OldHierarchy,KeepAggInfo,Reusable,Repartition,Aux;
  PetscBool         reuse_interpolation;
  //PCMLNullSpaceType nulltype;
  PetscMPIInt       size; /* size of communicator for pc->pmat */
  PetscInt          dim;  /* data from PCSetCoordinates(_ML) */
  PetscInt          nloc;
  PetscReal         *coords; /* ML has a grid object for each level: the finest grid will point into coords */
} PC_RAPTOR;



/* -----------------------------------------------------------------------------*/

extern PetscErrorCode PCReset_MG(PC);

PetscErrorCode PCReset_RAPTOR(PC pc)
{
  PetscErrorCode ierr=0;
  
  PetscFunctionBegin;
  CHKERRQ(PCReset_MG(pc));
  PetscFunctionReturn(ierr);
}
/* -------------------------------------------------------------------------- */
/*
   PCSetUp_RAPTOR - Prepares for the use of the RAPTOR preconditioner
                    by setting data structures and options.

   Input Parameter:
.  pc - the preconditioner context

   Application Interface Routine: PCSetUp()

   Notes:
   The interface routine PCSetUp() is not usually called directly by
   the user, but instead is called by PCApply() if necessary.
*/

PetscErrorCode PCSetUp_RAPTOR(PC pc)
{
  PetscErrorCode    ierr=0;
  
  PetscFunctionBegin;
  PetscFunctionReturn(ierr);
}

/* -------------------------------------------------------------------------- */
/*
   PCDestroy_RAPTOR - Destroys the private context for the ML preconditioner
   that was created with PCCreate_RAPTOR().

   Input Parameter:
.  pc - the preconditioner context

   Application Interface Routine: PCDestroy()
*/
PetscErrorCode PCDestroy_RAPTOR(PC pc)
{
  PetscErrorCode    ierr=0;
  
  PetscFunctionBegin;
  
  PC_MG          *mg        = (PC_MG*)pc->data;
  PC_RAPTOR      *pc_raptor = (PC_RAPTOR*)mg->innerctx;

  PetscFunctionBegin;
  
  CHKERRQ(PCReset_RAPTOR(pc));
  CHKERRQ(PetscFree(pc_raptor));
  CHKERRQ(PCDestroy_MG(pc));
  CHKERRQ(PetscObjectComposeFunction((PetscObject)pc,"PCSetCoordinates_C",NULL));
  
  PetscFunctionReturn(ierr);
  
}

PetscErrorCode PCSetFromOptions_RAPTOR(PetscOptionItems *PetscOptionsObject,PC pc)
{
  PetscErrorCode    ierr=0;
  
  PetscFunctionBegin;
  PetscFunctionReturn(ierr);
}





PETSC_EXTERN PetscErrorCode PCCreate_RAPTOR(PC pc)
{

  PetscErrorCode    ierr=0;
  PC_RAPTOR      *pc_raptor;
  PC_MG          *mg;
  
  PetscFunctionBegin;
  /* PCRAPTOR is an inherited class of PCMG. Initialize pc as PCMG */
  CHKERRQ(PCSetType(pc,PCMG)); /* calls PCCreate_MG() and MGCreate_Private() */
  CHKERRQ(PetscObjectChangeTypeName((PetscObject)pc,PCRAPTOR));
  /* Since PCMG tries to use DM assocated with PC must delete it */
  CHKERRQ(DMDestroy(&pc->dm));
  CHKERRQ(PCMGSetGalerkin(pc,PC_MG_GALERKIN_EXTERNAL));
  mg   = (PC_MG*)pc->data;

  /* create a supporting struct and attach it to pc */
  CHKERRQ(PetscNewLog(pc,&pc_raptor));
  mg->innerctx = pc_raptor;
  
//  pc_raptor->ml_object                = 0;
//  pc_raptor->agg_object               = 0;
  pc_raptor->gridctx                  = 0;
  pc_raptor->PetscRAPTORdata          = 0;
  pc_raptor->Nlevels                  = -1;
  pc_raptor->MaxNlevels               = 10;
  pc_raptor->MaxCoarseSize            = 1;
  pc_raptor->CoarsenScheme            = 1;
  pc_raptor->Threshold                = 0.0;
  pc_raptor->DampingFactor            = 4.0/3.0;
  pc_raptor->SpectralNormScheme_Anorm = PETSC_FALSE;
  pc_raptor->size                     = 0;
  pc_raptor->dim                      = 0;
  pc_raptor->nloc                     = 0;
  pc_raptor->coords                   = 0;
  pc_raptor->Repartition              = PETSC_FALSE;
  pc_raptor->MaxMinRatio              = 1.3;
  pc_raptor->MinPerProc               = 512;
  pc_raptor->PutOnSingleProc          = 5000;
  pc_raptor->RepartitionType          = 0;
  pc_raptor->ZoltanScheme             = 0;
  pc_raptor->Aux                      = PETSC_FALSE;
  pc_raptor->AuxThreshold             = 0.0;
  
  

  /* overwrite the pointers of PCMG by the functions of PCRAPTOR */
  pc->ops->setfromoptions = PCSetFromOptions_RAPTOR;
  pc->ops->setup          = PCSetUp_RAPTOR;
  pc->ops->reset          = PCReset_RAPTOR;
  pc->ops->destroy        = PCDestroy_RAPTOR;


  cout << "begining to implement 2\n\n";

  MPI_Finalize();
  exit(ierr);
  
  PetscFunctionReturn(ierr);
}

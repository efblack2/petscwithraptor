Configure petsc with raptor using: (adjust the path as needed.)

      ./configure --with-clanguage=cxx CXXOPTFLAGS='-O3 -std=c++11' --with-raptor-include=$PETSC_DIR/raptor/include --with-raptor-lib="-L$PETSC_DIR/raptor/lib -lraptor"   

Note: For now, the raptor/include and a raptor/lib directories inside $PETSC_DIR.
      in the configure command shown above, $PETSC_DIR/raptor/include and $PETSC_DIR/raptor/lib 
      indicate the location of the raptor static library (libraptor.a) and raptor headers.

